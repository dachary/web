---
date: 2019-14-15
---
## Qu'est ce qu'un tisseur?

Dans cette ère se joue une guerre des corporations contre les [internautes](http://editions-hache.com/essais/barlow/barlow2.html), pour la captation et le contrôle des informations, des données, et de leurs recoupements. A vrai dire depuis le début de l'internet il y a déjà eu plusieurs batailles, pour le droit à la cryptographie, les histoires de copyright, la régulation de l'information, etc. Les lobbies font pression pour marchandiser notre vie numérique. Et notre devoir est de résister.

Le modèle des [SaaS (Software as a Service)](https://fr.wikipedia.org/wiki/Logiciel_en_tant_que_service) déporte les applications sur des serveurs en ligne plutôt que de les proposer à l'installation locale. Le combat du [Logiciel Libre](https://www.gnu.org/philosophy/free-sw.fr.html) a donc changé de terrain. Un service hébergé, vous n'en verrez jamais le code source (à moins de vous faire embaucher par la corpo qui le fournit). Personne ne peut contrôler ce qui est fait de l'information collectée. La bataille est maintenant infrastructurelle.

Une parade, connue sous le nom de [dégafamisation](https://degooglisons-internet.org/fr/), consiste en la ré-appropriation de l’infrastructure de nos outils de communication et de collaboration. L'association [Framasoft](https//framsoft.org) y travaille avec courage depuis des années. Leur [catalogue](https://framasoft.org/fr/full/), documentation et espace d’expérimentation, sont de grande qualité. Les [Chatons](https://chatons.org) font partie de cette initiative, et définissent une norme éthique suivie par les groupes qui fournissent un service d'hébergement citoyen.

Dans cette démarche de redomiciliation de services, il y a des gens qui, soit par expérience soit par audace, prennent sur eux de louer un serveur quelque part (voire même à la maison), et d'héberger un service pour un groupe auquel ils appartiennent. Un peu comme les chatons mais à titre individuel et pour les besoins d'un groupe précis.

Ces gens, ce sont les tisseurs et les tisseuses. Il fabriquent la toile. Précisons bien que les tisseurs ne se distinguent pas par leur expertise mais par le résultat de leur action. Certains n'ont pas une idée claire de ce qu'ils font, mais le résultat est la: un groupe dispose d'alternatives aux voleurs de données.

Les Tisseurs, ce sont des citoyens-batisseurs des communs numérique.

## Le but du site des Tisseurs

Souvent les tisseurs souffrent d'isolement. En particulier quand ils débutent. Ce collectif de tisseurs a pour but de:

- connecter les tisseurs pour leur permettre de se regrouper ponctuellement ou pour un plus long terme
- définir un [Code d'Honneur](/code_honneur/) lié aux responsabilités inhérentes au rôle de tisseur
- définir des standards de qualité et de sécurité pertinents et assurer la propagation d'un bon sens commun en terme de mesures de sécurité et de disponibilité de service
- centraliser des informations pour apprendre l'administration système courante, favoriser l’échange de savoir-faire de tissage, et démystifier l'art du tissage numérique.

## Comment participer

- Déclarez votre adhésion au [Code d'Honneur des Tisseurs](/code_honneur/) publiquement comme un engagement moral de probité et de rectitude.
- Parcourez notre [Centre de Ressources](/ressources/) pour découvrir comment il pourrait être simple de devenir Tisseur pour les groupes auxquels vous participez.
